package de.viada.service;

import javax.ws.rs.core.Response;

import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class BirthDayEndpoint {

		private Logger logDog = Logger.getLogger(BirthDayEndpoint.class.getName());
		
		@Path("/")
		@GET
		@Produces(MediaType.TEXT_PLAIN)
		public Response helloWorld() {
			logDog.info("Hello World Endpoint called");
			return Response.ok("Hello dear world!").build();
		}
}
